/*
 Navicat Premium Data Transfer

 Source Server         : local-link-mysql
 Source Server Type    : MySQL
 Source Server Version : 50638
 Source Host           : localhost:3306
 Source Schema         : munitions

 Target Server Type    : MySQL
 Target Server Version : 50638
 File Encoding         : 65001

 Date: 11/07/2021 09:11:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrator
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of administrator
-- ----------------------------
INSERT INTO `administrator` VALUES (1, '洛必达', '123456789');
INSERT INTO `administrator` VALUES (2, '莱布尼茨', '123456');
INSERT INTO `administrator` VALUES (3, 'zhangsan', '320826');

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `province`(`province`) USING BTREE,
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`province`) REFERENCES `province` (`province`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 412 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES (1, '石家庄 ', '河北');
INSERT INTO `city` VALUES (2, '沧州 ', '河北');
INSERT INTO `city` VALUES (3, '承德 ', '河北');
INSERT INTO `city` VALUES (4, '秦皇岛 ', '河北');
INSERT INTO `city` VALUES (5, '唐山 ', '河北');
INSERT INTO `city` VALUES (6, '保定 ', '河北');
INSERT INTO `city` VALUES (7, '廊坊 ', '河北');
INSERT INTO `city` VALUES (8, '邢台 ', '河北');
INSERT INTO `city` VALUES (9, '衡水', '河北');
INSERT INTO `city` VALUES (10, '张家口', '河北');
INSERT INTO `city` VALUES (11, '邯郸 ', '河北');
INSERT INTO `city` VALUES (12, '任丘', '河北');
INSERT INTO `city` VALUES (13, '河间 ', '河北');
INSERT INTO `city` VALUES (14, ' 泊头', '河北');
INSERT INTO `city` VALUES (15, '太原 ', '山西');
INSERT INTO `city` VALUES (16, '长治 ', '山西');
INSERT INTO `city` VALUES (17, '大同 ', '山西');
INSERT INTO `city` VALUES (18, '阳泉 ', '山西');
INSERT INTO `city` VALUES (19, '朔州 ', '山西');
INSERT INTO `city` VALUES (20, '临汾', '山西');
INSERT INTO `city` VALUES (21, '晋城', '山西');
INSERT INTO `city` VALUES (22, ' 忻州', '山西');
INSERT INTO `city` VALUES (23, '运城', '山西');
INSERT INTO `city` VALUES (24, '晋中', '山西');
INSERT INTO `city` VALUES (25, '吕梁', '山西');
INSERT INTO `city` VALUES (26, '呼和浩特', '内蒙');
INSERT INTO `city` VALUES (32, '包头', '内蒙');
INSERT INTO `city` VALUES (33, '赤峰', '内蒙');
INSERT INTO `city` VALUES (34, '鄂尔多斯', '内蒙');
INSERT INTO `city` VALUES (35, '呼伦贝尔', '内蒙');
INSERT INTO `city` VALUES (36, '巴彦淖尔', '内蒙');
INSERT INTO `city` VALUES (37, '阿拉善', '内蒙');
INSERT INTO `city` VALUES (38, '兴安', '内蒙');
INSERT INTO `city` VALUES (39, '通辽', '内蒙');
INSERT INTO `city` VALUES (40, '乌海', '内蒙');
INSERT INTO `city` VALUES (41, '乌兰察布', '内蒙');
INSERT INTO `city` VALUES (42, NULL, '内蒙');
INSERT INTO `city` VALUES (43, '锡林郭勒', '内蒙');
INSERT INTO `city` VALUES (44, '沈阳', '辽宁');
INSERT INTO `city` VALUES (45, '大连', '辽宁');
INSERT INTO `city` VALUES (46, '本溪', '辽宁');
INSERT INTO `city` VALUES (47, '阜新', '辽宁');
INSERT INTO `city` VALUES (48, '葫芦岛', '辽宁');
INSERT INTO `city` VALUES (49, '盘锦', '辽宁');
INSERT INTO `city` VALUES (50, '铁峰', '辽宁');
INSERT INTO `city` VALUES (51, '丹东', '辽宁');
INSERT INTO `city` VALUES (52, '锦州', '辽宁');
INSERT INTO `city` VALUES (53, '营口', '辽宁');
INSERT INTO `city` VALUES (54, '鞍山', '辽宁');
INSERT INTO `city` VALUES (55, '辽阳', '辽宁');
INSERT INTO `city` VALUES (56, '抚顺', '辽宁');
INSERT INTO `city` VALUES (57, '朝阳', '辽宁');
INSERT INTO `city` VALUES (58, '长春', '吉林');
INSERT INTO `city` VALUES (59, '白城', '吉林');
INSERT INTO `city` VALUES (60, '白山', '吉林');
INSERT INTO `city` VALUES (61, '吉林', '吉林');
INSERT INTO `city` VALUES (62, '辽源', '吉林');
INSERT INTO `city` VALUES (63, '四平', '吉林');
INSERT INTO `city` VALUES (64, '通化', '吉林');
INSERT INTO `city` VALUES (65, '松原', '吉林');
INSERT INTO `city` VALUES (66, '延边', '吉林');
INSERT INTO `city` VALUES (67, '珲春', '吉林');
INSERT INTO `city` VALUES (68, '龙井', '吉林');
INSERT INTO `city` VALUES (69, '舒兰', '吉林');
INSERT INTO `city` VALUES (70, '哈尔滨 ', '黑龙江');
INSERT INTO `city` VALUES (71, '大庆 ', '黑龙江');
INSERT INTO `city` VALUES (72, '大兴安岭', '黑龙江');
INSERT INTO `city` VALUES (74, '鹤岗', '黑龙江');
INSERT INTO `city` VALUES (75, '黑河', '黑龙江');
INSERT INTO `city` VALUES (76, '鸡西', '黑龙江');
INSERT INTO `city` VALUES (77, '佳木斯', '黑龙江');
INSERT INTO `city` VALUES (78, '牡丹江', '黑龙江');
INSERT INTO `city` VALUES (79, '七台河', '黑龙江');
INSERT INTO `city` VALUES (80, '双鸭山', '黑龙江');
INSERT INTO `city` VALUES (81, '齐齐哈尔', '黑龙江');
INSERT INTO `city` VALUES (82, '伊春', '黑龙江');
INSERT INTO `city` VALUES (83, ' 绥化', '黑龙江');
INSERT INTO `city` VALUES (84, '虎林 ', '黑龙江');
INSERT INTO `city` VALUES (85, '五常 ', '黑龙江');
INSERT INTO `city` VALUES (86, '密山', '黑龙江');
INSERT INTO `city` VALUES (87, '宁安', '黑龙江');
INSERT INTO `city` VALUES (88, '南京', '江苏');
INSERT INTO `city` VALUES (89, '苏州', '江苏');
INSERT INTO `city` VALUES (90, '扬州', '江苏');
INSERT INTO `city` VALUES (91, '无锡', '江苏');
INSERT INTO `city` VALUES (92, '南通', '江苏');
INSERT INTO `city` VALUES (93, '常州', '江苏');
INSERT INTO `city` VALUES (94, '连云港', '江苏');
INSERT INTO `city` VALUES (95, '徐州', '江苏');
INSERT INTO `city` VALUES (96, '镇江', '江苏');
INSERT INTO `city` VALUES (97, '淮安', '江苏');
INSERT INTO `city` VALUES (98, '宿迁', '江苏');
INSERT INTO `city` VALUES (99, '泰州', '江苏');
INSERT INTO `city` VALUES (100, '盐城', '江苏');
INSERT INTO `city` VALUES (101, '杭州', '浙江');
INSERT INTO `city` VALUES (102, '宁波', '浙江');
INSERT INTO `city` VALUES (103, '温州', '浙江');
INSERT INTO `city` VALUES (104, '丽水', '浙江');
INSERT INTO `city` VALUES (105, '奉化', '浙江');
INSERT INTO `city` VALUES (106, '宁海', '浙江');
INSERT INTO `city` VALUES (107, '临海', '浙江');
INSERT INTO `city` VALUES (108, '三门', '浙江');
INSERT INTO `city` VALUES (109, '绍兴', '浙江');
INSERT INTO `city` VALUES (110, '舟山', '浙江');
INSERT INTO `city` VALUES (111, '义乌', '浙江');
INSERT INTO `city` VALUES (112, '北仑', '浙江');
INSERT INTO `city` VALUES (113, '慈溪', '浙江');
INSERT INTO `city` VALUES (114, '象山', '浙江');
INSERT INTO `city` VALUES (115, '余姚', '浙江');
INSERT INTO `city` VALUES (116, '天台', '浙江');
INSERT INTO `city` VALUES (117, '温岭', '浙江');
INSERT INTO `city` VALUES (118, '仙居', '浙江');
INSERT INTO `city` VALUES (119, '台州', '浙江');
INSERT INTO `city` VALUES (120, '嘉兴', '浙江');
INSERT INTO `city` VALUES (121, '湖州', '浙江');
INSERT INTO `city` VALUES (122, '衢州', '浙江');
INSERT INTO `city` VALUES (123, '金华', '浙江');
INSERT INTO `city` VALUES (124, '合肥', '安徽');
INSERT INTO `city` VALUES (125, '黄山', '安徽');
INSERT INTO `city` VALUES (126, '芜湖', '安徽');
INSERT INTO `city` VALUES (127, '铜陵', '安徽');
INSERT INTO `city` VALUES (128, '安庆', '安徽');
INSERT INTO `city` VALUES (129, '滁州', '安徽');
INSERT INTO `city` VALUES (130, '宣城', '安徽');
INSERT INTO `city` VALUES (131, '阜阳', '安徽');
INSERT INTO `city` VALUES (132, '淮北', '安徽');
INSERT INTO `city` VALUES (133, '蚌埠', '安徽');
INSERT INTO `city` VALUES (134, '池州', '安徽');
INSERT INTO `city` VALUES (135, '青阳', '安徽');
INSERT INTO `city` VALUES (136, '九华山景区', '安徽');
INSERT INTO `city` VALUES (137, '黄山景区', '安徽');
INSERT INTO `city` VALUES (138, '巢湖', '安徽');
INSERT INTO `city` VALUES (139, '亳州', '安徽');
INSERT INTO `city` VALUES (140, '马鞍山', '安徽');
INSERT INTO `city` VALUES (141, '宿州', '安徽');
INSERT INTO `city` VALUES (142, '六安', '安徽');
INSERT INTO `city` VALUES (143, '淮南', '安徽');
INSERT INTO `city` VALUES (144, '福州', '福建');
INSERT INTO `city` VALUES (145, '厦门', '福建');
INSERT INTO `city` VALUES (146, '泉州', '福建');
INSERT INTO `city` VALUES (147, '漳州', '福建');
INSERT INTO `city` VALUES (148, '龙岩', '福建');
INSERT INTO `city` VALUES (149, '三明', '福建');
INSERT INTO `city` VALUES (150, '南平', '福建');
INSERT INTO `city` VALUES (151, '永安', '福建');
INSERT INTO `city` VALUES (152, '宁德', '福建');
INSERT INTO `city` VALUES (153, '莆田', '福建');
INSERT INTO `city` VALUES (154, '南昌', '江西');
INSERT INTO `city` VALUES (155, '九江', '江西');
INSERT INTO `city` VALUES (156, '赣州', '江西');
INSERT INTO `city` VALUES (157, '景德镇', '江西');
INSERT INTO `city` VALUES (158, '萍乡', '江西');
INSERT INTO `city` VALUES (159, '新余', '江西');
INSERT INTO `city` VALUES (160, '吉安', '江西');
INSERT INTO `city` VALUES (161, '宜春', '江西');
INSERT INTO `city` VALUES (162, '抚州', '江西');
INSERT INTO `city` VALUES (163, '上饶', '江西');
INSERT INTO `city` VALUES (164, '鹰潭', '江西');
INSERT INTO `city` VALUES (165, '济南', '山东');
INSERT INTO `city` VALUES (166, '青岛', '山东');
INSERT INTO `city` VALUES (167, '烟台', '山东');
INSERT INTO `city` VALUES (168, '威海', '山东');
INSERT INTO `city` VALUES (169, '潍坊', '山东');
INSERT INTO `city` VALUES (170, '德州', '山东');
INSERT INTO `city` VALUES (171, '滨州', '山东');
INSERT INTO `city` VALUES (172, '东营', '山东');
INSERT INTO `city` VALUES (173, '聊城', '山东');
INSERT INTO `city` VALUES (174, '菏泽', '山东');
INSERT INTO `city` VALUES (175, '济宁', '山东');
INSERT INTO `city` VALUES (176, '临沂', '山东');
INSERT INTO `city` VALUES (177, '淄博', '山东');
INSERT INTO `city` VALUES (178, '泰安', '山东');
INSERT INTO `city` VALUES (179, '枣庄', '山东');
INSERT INTO `city` VALUES (180, '日照', '山东');
INSERT INTO `city` VALUES (181, '莱芜', '山东');
INSERT INTO `city` VALUES (182, '海阳', '山东');
INSERT INTO `city` VALUES (183, '平度', '山东');
INSERT INTO `city` VALUES (184, '青州', '山东');
INSERT INTO `city` VALUES (185, '肥城', '山东');
INSERT INTO `city` VALUES (188, '郑州', '河南');
INSERT INTO `city` VALUES (189, '安阳', '河南');
INSERT INTO `city` VALUES (190, '济源', '河南');
INSERT INTO `city` VALUES (191, '鹤壁', '河南');
INSERT INTO `city` VALUES (192, '焦作', '河南');
INSERT INTO `city` VALUES (193, '开封', '河南');
INSERT INTO `city` VALUES (194, '濮阳', '河南');
INSERT INTO `city` VALUES (195, '三门峡', '河南');
INSERT INTO `city` VALUES (196, '驻马店', '河南');
INSERT INTO `city` VALUES (197, '商丘', '河南');
INSERT INTO `city` VALUES (198, '新乡', '河南');
INSERT INTO `city` VALUES (199, '信阳', '河南');
INSERT INTO `city` VALUES (200, '许昌', '河南');
INSERT INTO `city` VALUES (201, '周口', '河南');
INSERT INTO `city` VALUES (202, '南阳', '河南');
INSERT INTO `city` VALUES (203, '洛阳', '河南');
INSERT INTO `city` VALUES (204, '平顶山', '河南');
INSERT INTO `city` VALUES (205, '漯河', '河南');
INSERT INTO `city` VALUES (206, '武汉', '湖北');
INSERT INTO `city` VALUES (207, '十堰', '湖北');
INSERT INTO `city` VALUES (208, '宜昌', '湖北');
INSERT INTO `city` VALUES (209, '鄂州', '湖北');
INSERT INTO `city` VALUES (210, '黄石', '湖北');
INSERT INTO `city` VALUES (211, '襄樊', '湖北');
INSERT INTO `city` VALUES (212, '荆州', '湖北');
INSERT INTO `city` VALUES (213, '荆门', '湖北');
INSERT INTO `city` VALUES (214, '孝感', '湖北');
INSERT INTO `city` VALUES (215, '黄冈', '湖北');
INSERT INTO `city` VALUES (216, '咸宁', '湖北');
INSERT INTO `city` VALUES (217, '随州', '湖北');
INSERT INTO `city` VALUES (218, '恩施', '湖北');
INSERT INTO `city` VALUES (219, '仙桃', '湖北');
INSERT INTO `city` VALUES (220, '天门', '湖北');
INSERT INTO `city` VALUES (221, '潜江', '湖北');
INSERT INTO `city` VALUES (222, '长沙', '湖南');
INSERT INTO `city` VALUES (223, '张家界', '湖南');
INSERT INTO `city` VALUES (224, '株洲', '湖南');
INSERT INTO `city` VALUES (225, '韶山', '湖南');
INSERT INTO `city` VALUES (226, '衡阳', '湖南');
INSERT INTO `city` VALUES (227, '郴州', '湖南');
INSERT INTO `city` VALUES (228, '冷水江', '湖南');
INSERT INTO `city` VALUES (229, '娄底', '湖南');
INSERT INTO `city` VALUES (230, '耒阳', '湖南');
INSERT INTO `city` VALUES (231, '永州', '湖南');
INSERT INTO `city` VALUES (232, '湘江', '湖南');
INSERT INTO `city` VALUES (233, '常德', '湖南');
INSERT INTO `city` VALUES (234, '益阳', '湖南');
INSERT INTO `city` VALUES (235, '怀化', '湖南');
INSERT INTO `city` VALUES (236, '邵阳', '湖南');
INSERT INTO `city` VALUES (237, '岳阳', '湖南');
INSERT INTO `city` VALUES (238, '吉首', '湖南');
INSERT INTO `city` VALUES (240, '广州', '广东');
INSERT INTO `city` VALUES (241, '深圳', '广东');
INSERT INTO `city` VALUES (242, '珠海', '广东');
INSERT INTO `city` VALUES (243, '东莞', '广东');
INSERT INTO `city` VALUES (244, '佛山', '广东');
INSERT INTO `city` VALUES (245, '潮州', '广东');
INSERT INTO `city` VALUES (246, '番禺', '广东');
INSERT INTO `city` VALUES (247, '汕头', '广东');
INSERT INTO `city` VALUES (248, '湛江', '广东');
INSERT INTO `city` VALUES (249, '中山', '广东');
INSERT INTO `city` VALUES (250, '惠州', '广东');
INSERT INTO `city` VALUES (251, '河源', '广东');
INSERT INTO `city` VALUES (252, '揭阳', '广东');
INSERT INTO `city` VALUES (253, '梅州', '广东');
INSERT INTO `city` VALUES (254, '肇庆', '广东');
INSERT INTO `city` VALUES (255, '茂名', '广东');
INSERT INTO `city` VALUES (256, '云浮', '广东');
INSERT INTO `city` VALUES (257, '阳江', '广东');
INSERT INTO `city` VALUES (258, '江门', '广东');
INSERT INTO `city` VALUES (259, '韶关', '广东');
INSERT INTO `city` VALUES (260, '乐昌', '广东');
INSERT INTO `city` VALUES (261, '化州', '广东');
INSERT INTO `city` VALUES (262, '从化', '广东');
INSERT INTO `city` VALUES (263, '鹤山', '广东');
INSERT INTO `city` VALUES (264, '汕尾', '广东');
INSERT INTO `city` VALUES (265, '清远', '广东');
INSERT INTO `city` VALUES (266, '南宁', '广西');
INSERT INTO `city` VALUES (267, '柳州', '广西');
INSERT INTO `city` VALUES (268, '北海', '广西');
INSERT INTO `city` VALUES (269, '百色', '广西');
INSERT INTO `city` VALUES (270, '梧州', '广西');
INSERT INTO `city` VALUES (271, '贺州', '广西');
INSERT INTO `city` VALUES (272, '玉林', '广西');
INSERT INTO `city` VALUES (273, '河池', '广西');
INSERT INTO `city` VALUES (274, '桂林', '广西');
INSERT INTO `city` VALUES (275, '钦州', '广西');
INSERT INTO `city` VALUES (276, '防城港', '广西');
INSERT INTO `city` VALUES (277, '来宾', '广西');
INSERT INTO `city` VALUES (278, '崇左', '广西');
INSERT INTO `city` VALUES (279, '贵港', '广西');
INSERT INTO `city` VALUES (280, '北流', '广西');
INSERT INTO `city` VALUES (281, '宜州', '广西');
INSERT INTO `city` VALUES (282, '桂平', '广西');
INSERT INTO `city` VALUES (283, '海口', '海南');
INSERT INTO `city` VALUES (284, '三亚', '海南');
INSERT INTO `city` VALUES (285, '五指山', '海南');
INSERT INTO `city` VALUES (286, '琼海', '海南');
INSERT INTO `city` VALUES (287, '儋州', '海南');
INSERT INTO `city` VALUES (288, '文昌', '海南');
INSERT INTO `city` VALUES (289, '万宁', '海南');
INSERT INTO `city` VALUES (290, '东方', '海南');
INSERT INTO `city` VALUES (291, '成都', '四川');
INSERT INTO `city` VALUES (292, '内江', '四川');
INSERT INTO `city` VALUES (293, '峨眉山', '四川');
INSERT INTO `city` VALUES (294, '绵阳', '四川');
INSERT INTO `city` VALUES (295, '宜宾', '四川');
INSERT INTO `city` VALUES (296, '泸州', '四川');
INSERT INTO `city` VALUES (297, '攀枝花', '四川');
INSERT INTO `city` VALUES (298, '自贡', '四川');
INSERT INTO `city` VALUES (299, '资阳', '四川');
INSERT INTO `city` VALUES (300, '崇州', '四川');
INSERT INTO `city` VALUES (301, '西昌', '四川');
INSERT INTO `city` VALUES (302, '都江堰', '四川');
INSERT INTO `city` VALUES (303, '遂宁', '四川');
INSERT INTO `city` VALUES (304, '乐山', '四川');
INSERT INTO `city` VALUES (305, '达州', '四川');
INSERT INTO `city` VALUES (306, '江油', '四川');
INSERT INTO `city` VALUES (307, '大邑', '四川');
INSERT INTO `city` VALUES (308, '金堂', '四川');
INSERT INTO `city` VALUES (309, '德阳', '四川');
INSERT INTO `city` VALUES (310, '南充', '四川');
INSERT INTO `city` VALUES (311, '广安', '四川');
INSERT INTO `city` VALUES (312, '广元', '四川');
INSERT INTO `city` VALUES (313, '巴中', '四川');
INSERT INTO `city` VALUES (314, '雅安', '四川');
INSERT INTO `city` VALUES (315, '贵阳', '贵州');
INSERT INTO `city` VALUES (316, '安顺', '贵州');
INSERT INTO `city` VALUES (317, '铜仁', '贵州');
INSERT INTO `city` VALUES (318, '六盘水', '贵州');
INSERT INTO `city` VALUES (319, '遵义', '贵州');
INSERT INTO `city` VALUES (320, '毕节', '贵州');
INSERT INTO `city` VALUES (321, '兴义', '贵州');
INSERT INTO `city` VALUES (322, '凯里', '贵州');
INSERT INTO `city` VALUES (323, '都匀', '贵州');
INSERT INTO `city` VALUES (324, '昆明', '云南');
INSERT INTO `city` VALUES (325, '西双版纳', '云南');
INSERT INTO `city` VALUES (326, '大理', '云南');
INSERT INTO `city` VALUES (327, '德宏', '云南');
INSERT INTO `city` VALUES (328, '思茅', '云南');
INSERT INTO `city` VALUES (329, '曲靖', '云南');
INSERT INTO `city` VALUES (330, '玉溪', '云南');
INSERT INTO `city` VALUES (331, '保山', '云南');
INSERT INTO `city` VALUES (332, '昭通', '云南');
INSERT INTO `city` VALUES (333, '临沧', '云南');
INSERT INTO `city` VALUES (334, '丽江', '云南');
INSERT INTO `city` VALUES (335, '文山', '云南');
INSERT INTO `city` VALUES (336, '个旧', '云南');
INSERT INTO `city` VALUES (337, '楚雄', '云南');
INSERT INTO `city` VALUES (338, '香格里拉', '云南');
INSERT INTO `city` VALUES (339, '怒江', '云南');
INSERT INTO `city` VALUES (340, '拉萨', '西藏');
INSERT INTO `city` VALUES (341, '那曲', '西藏');
INSERT INTO `city` VALUES (342, '昌都', '西藏');
INSERT INTO `city` VALUES (343, '山南', '西藏');
INSERT INTO `city` VALUES (344, '日喀则', '西藏');
INSERT INTO `city` VALUES (345, '阿里', '西藏');
INSERT INTO `city` VALUES (346, '林芝', '西藏');
INSERT INTO `city` VALUES (347, '商洛', '陕西');
INSERT INTO `city` VALUES (348, '西安', '陕西');
INSERT INTO `city` VALUES (349, '宝鸡', '陕西');
INSERT INTO `city` VALUES (350, '延安', '陕西');
INSERT INTO `city` VALUES (351, '兴平', '陕西');
INSERT INTO `city` VALUES (352, '咸阳', '陕西');
INSERT INTO `city` VALUES (353, '铜川', '陕西');
INSERT INTO `city` VALUES (354, '渭南', '陕西');
INSERT INTO `city` VALUES (355, '汉中', '陕西');
INSERT INTO `city` VALUES (356, '榆林', '陕西');
INSERT INTO `city` VALUES (357, '安康', '陕西');
INSERT INTO `city` VALUES (358, '兰州', '甘肃');
INSERT INTO `city` VALUES (359, '嘉峪关', '甘肃');
INSERT INTO `city` VALUES (360, '酒泉', '甘肃');
INSERT INTO `city` VALUES (361, '临夏', '甘肃');
INSERT INTO `city` VALUES (362, '白银', '甘肃');
INSERT INTO `city` VALUES (363, '天水', '甘肃');
INSERT INTO `city` VALUES (364, '武威', '甘肃');
INSERT INTO `city` VALUES (365, '张掖', '甘肃');
INSERT INTO `city` VALUES (366, '平凉', '甘肃');
INSERT INTO `city` VALUES (367, '庆阳', '甘肃');
INSERT INTO `city` VALUES (368, '定西', '甘肃');
INSERT INTO `city` VALUES (369, '陇南', '甘肃');
INSERT INTO `city` VALUES (370, '甘南', '甘肃');
INSERT INTO `city` VALUES (371, '西宁', '青海');
INSERT INTO `city` VALUES (372, '海东', '青海');
INSERT INTO `city` VALUES (373, '海北', '青海');
INSERT INTO `city` VALUES (374, '黄南', '青海');
INSERT INTO `city` VALUES (375, '海南', '青海');
INSERT INTO `city` VALUES (376, '果洛', '青海');
INSERT INTO `city` VALUES (377, '海西', '青海');
INSERT INTO `city` VALUES (378, '玉树', '青海');
INSERT INTO `city` VALUES (379, '银川', '宁夏');
INSERT INTO `city` VALUES (380, '石嘴山', '宁夏');
INSERT INTO `city` VALUES (381, '吴忠', '宁夏');
INSERT INTO `city` VALUES (382, '固原 ', '宁夏');
INSERT INTO `city` VALUES (383, '中卫', '宁夏');
INSERT INTO `city` VALUES (384, '乌鲁木齐 ', '新疆');
INSERT INTO `city` VALUES (385, '克拉玛依', '新疆');
INSERT INTO `city` VALUES (386, '哈密 ', '新疆');
INSERT INTO `city` VALUES (387, '喀什 ', '新疆');
INSERT INTO `city` VALUES (388, '吐鲁番 ', '新疆');
INSERT INTO `city` VALUES (389, '石河子 ', '新疆');
INSERT INTO `city` VALUES (390, '图木舒克', '新疆');
INSERT INTO `city` VALUES (391, '和田 ', '新疆');
INSERT INTO `city` VALUES (392, '昌吉', '新疆');
INSERT INTO `city` VALUES (393, '阿图什', '新疆');
INSERT INTO `city` VALUES (394, '库尔勒', '新疆');
INSERT INTO `city` VALUES (395, '博乐', '新疆');
INSERT INTO `city` VALUES (396, '伊宁', '新疆');
INSERT INTO `city` VALUES (397, '阿拉尔', '新疆');
INSERT INTO `city` VALUES (398, '阿克苏', '新疆');
INSERT INTO `city` VALUES (399, '五家渠', '新疆');
INSERT INTO `city` VALUES (400, '台北 ', '台湾');
INSERT INTO `city` VALUES (401, '台中 ', '台湾');
INSERT INTO `city` VALUES (402, '台南 ', '台湾');
INSERT INTO `city` VALUES (403, '高雄', '台湾');
INSERT INTO `city` VALUES (404, '香港', '香港');
INSERT INTO `city` VALUES (405, '澳门', '澳门');
INSERT INTO `city` VALUES (407, '章丘', '山东');
INSERT INTO `city` VALUES (408, '天津', '天津');
INSERT INTO `city` VALUES (409, '上海', '上海');
INSERT INTO `city` VALUES (410, '重庆', '重庆');
INSERT INTO `city` VALUES (411, '北京', '北京');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `goods_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `goods_pic` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片路径',
  `org_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `now_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '现价',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comments` int(4) NULL DEFAULT 0 COMMENT '评论',
  `goods_num` int(11) NULL DEFAULT 100 COMMENT '库存',
  `buy_num` int(11) NULL DEFAULT 0 COMMENT '购买数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES (1, 'AKM', 'image/AKM.png', 3500.88, 2888.88, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (2, 'AR15', 'image/AR15.png', 2999.66, 2000.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (3, 'AUG', 'image/AUG.png', 3800.00, 3200.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (4, 'AWM', 'image/AWM.jpg', 18000.00, 12000.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (5, 'HK416', 'image/HK416.jpg', 6888.00, 5700.99, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (6, 'Kar98K', 'image/Kar98K.png', 8888.00, 8000.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (7, 'M249', 'image/M249.png', 5888.00, 2888.00, 'Machine Gun', 0, 100, 0);
INSERT INTO `goods` VALUES (8, 'M416', 'image/M416.png', 7888.00, 6588.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (9, 'M762', 'image/M762.png', 6888.00, 4888.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (10, 'MK14', 'image/MK14.png', 9988.00, 8999.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (11, 'QBZ', 'image/QBZ.png', 6888.00, 5789.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (12, 'RPG', 'image/RPG.png', 13888.00, 10000.00, 'Missile', 0, 100, 0);
INSERT INTO `goods` VALUES (13, 'Scar', 'image/Scar.jpg', 15888.00, 12888.00, 'Assault Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (14, 'SKS', 'image/SKS.jpg', 14789.00, 6791.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (15, 'Sting', 'image/Sting.png', 58888.00, 48888.00, 'Missile', 0, 100, 0);
INSERT INTO `goods` VALUES (16, 'UMP9', 'image/UMP9.png', 1888.00, 1258.00, 'SubMachine Gun', 0, 100, 0);
INSERT INTO `goods` VALUES (17, 'Vector', 'image/Vector.png', 4789.00, 3899.00, 'SubMachine Gun', 0, 100, 0);
INSERT INTO `goods` VALUES (18, 'M24', 'image/M24.png', 10000.00, 8000.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (19, 'M24', 'image/M24.png', 10000.00, 8000.00, 'Sniper Rifle', 0, 100, 0);
INSERT INTO `goods` VALUES (20, 'M24', 'image/M24.png', 10000.00, 8000.00, 'Sniper Rifle', 0, 100, 0);

-- ----------------------------
-- Table structure for orderdetail
-- ----------------------------
DROP TABLE IF EXISTS `orderdetail`;
CREATE TABLE `orderdetail`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `goodsNo` int(6) NOT NULL,
  `nowPrice` double(10, 2) NULL DEFAULT NULL,
  `buyNum` int(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `orderId`, `goodsNo`) USING BTREE,
  INDEX `goodsNo`(`goodsNo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orderdetail
-- ----------------------------
INSERT INTO `orderdetail` VALUES (1, 'a0df4fd9d73042b1927b29697ef3f60d', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (2, 'a0df4fd9d73042b1927b29697ef3f60d', 4, 12000.00, 2);
INSERT INTO `orderdetail` VALUES (3, 'a0df4fd9d73042b1927b29697ef3f60d', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (4, 'ad975b15c6b94d7b981173913e5918ee', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (5, 'ad975b15c6b94d7b981173913e5918ee', 3, 3200.00, 1);
INSERT INTO `orderdetail` VALUES (6, 'ad975b15c6b94d7b981173913e5918ee', 4, 12000.00, 1);
INSERT INTO `orderdetail` VALUES (7, 'ad975b15c6b94d7b981173913e5918ee', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (8, 'd0afc3eea8f64224af6603820b69560c', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (9, 'd0afc3eea8f64224af6603820b69560c', 3, 3200.00, 1);
INSERT INTO `orderdetail` VALUES (10, 'd0afc3eea8f64224af6603820b69560c', 4, 12000.00, 1);
INSERT INTO `orderdetail` VALUES (11, 'd0afc3eea8f64224af6603820b69560c', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (12, '58ca7e26cc8c4234929b0de0c0129132', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (13, '58ca7e26cc8c4234929b0de0c0129132', 3, 3200.00, 1);
INSERT INTO `orderdetail` VALUES (14, '58ca7e26cc8c4234929b0de0c0129132', 4, 12000.00, 2);
INSERT INTO `orderdetail` VALUES (15, '58ca7e26cc8c4234929b0de0c0129132', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (16, 'd7dd603bae714d998549f1cd31c71880', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (17, 'd7dd603bae714d998549f1cd31c71880', 2, 2000.00, 1);
INSERT INTO `orderdetail` VALUES (18, 'd7dd603bae714d998549f1cd31c71880', 4, 12000.00, 1);
INSERT INTO `orderdetail` VALUES (19, '414e08435bdd4d84a2f997297936703e', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (20, '414e08435bdd4d84a2f997297936703e', 9, 4888.00, 1);
INSERT INTO `orderdetail` VALUES (21, '6f1df5ec61a043a0adebabfb4f980f17', 1, 2888.88, 1);
INSERT INTO `orderdetail` VALUES (22, '6f1df5ec61a043a0adebabfb4f980f17', 2, 2000.00, 1);
INSERT INTO `orderdetail` VALUES (23, '6f1df5ec61a043a0adebabfb4f980f17', 5, 5700.99, 1);
INSERT INTO `orderdetail` VALUES (24, 'f6152a4a9ff5455491f97f50213e9cdf', 1, 2888.88, 9);
INSERT INTO `orderdetail` VALUES (25, 'f6152a4a9ff5455491f97f50213e9cdf', 2, 2000.00, 12);
INSERT INTO `orderdetail` VALUES (26, 'f6152a4a9ff5455491f97f50213e9cdf', 3, 3200.00, 11);
INSERT INTO `orderdetail` VALUES (27, 'f6152a4a9ff5455491f97f50213e9cdf', 4, 12000.00, 17);

-- ----------------------------
-- Table structure for orderlist
-- ----------------------------
DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist`  (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `orderId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `orderTime` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `totalPrice` double(30, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `orderId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orderlist
-- ----------------------------
INSERT INTO `orderlist` VALUES (1, 'a0df4fd9d73042b1927b29697ef3f60d', '洛必达', '2020-04-16 22:30:34', 32589.87);
INSERT INTO `orderlist` VALUES (2, 'ad975b15c6b94d7b981173913e5918ee', '洛必达', '2020-04-16 22:36:10', 23789.87);
INSERT INTO `orderlist` VALUES (3, 'd0afc3eea8f64224af6603820b69560c', '洛必达', '2020-04-16 22:49:32', 23789.87);
INSERT INTO `orderlist` VALUES (4, '58ca7e26cc8c4234929b0de0c0129132', '洛必达', '2020-04-16 22:53:09', 35789.87);
INSERT INTO `orderlist` VALUES (5, 'd7dd603bae714d998549f1cd31c71880', '洛必达', '2020-04-17 18:28:37', 16888.88);
INSERT INTO `orderlist` VALUES (6, '414e08435bdd4d84a2f997297936703e', '洛必达', '2020-04-21 09:29:10', 10588.99);
INSERT INTO `orderlist` VALUES (7, '6f1df5ec61a043a0adebabfb4f980f17', '洛必达', '2020-10-04 22:36:04', 10589.87);
INSERT INTO `orderlist` VALUES (8, 'f6152a4a9ff5455491f97f50213e9cdf', '洛必达', '2021-05-25 13:07:38', 289199.92);

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `province`(`province`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES (3, '上海');
INSERT INTO `province` VALUES (7, '云南');
INSERT INTO `province` VALUES (21, '内蒙');
INSERT INTO `province` VALUES (1, '北京');
INSERT INTO `province` VALUES (32, '台湾');
INSERT INTO `province` VALUES (23, '吉林');
INSERT INTO `province` VALUES (29, '四川');
INSERT INTO `province` VALUES (2, '天津');
INSERT INTO `province` VALUES (30, '宁夏');
INSERT INTO `province` VALUES (11, '安徽');
INSERT INTO `province` VALUES (12, '山东');
INSERT INTO `province` VALUES (20, '山西');
INSERT INTO `province` VALUES (26, '广东');
INSERT INTO `province` VALUES (18, '广西');
INSERT INTO `province` VALUES (13, '新疆');
INSERT INTO `province` VALUES (14, '江苏');
INSERT INTO `province` VALUES (16, '江西');
INSERT INTO `province` VALUES (5, '河北');
INSERT INTO `province` VALUES (6, '河南');
INSERT INTO `province` VALUES (15, '浙江');
INSERT INTO `province` VALUES (31, '海南');
INSERT INTO `province` VALUES (17, '湖北');
INSERT INTO `province` VALUES (10, '湖南');
INSERT INTO `province` VALUES (34, '澳门');
INSERT INTO `province` VALUES (19, '甘肃');
INSERT INTO `province` VALUES (24, '福建');
INSERT INTO `province` VALUES (28, '西藏');
INSERT INTO `province` VALUES (25, '贵州');
INSERT INTO `province` VALUES (8, '辽宁');
INSERT INTO `province` VALUES (4, '重庆');
INSERT INTO `province` VALUES (22, '陕西');
INSERT INTO `province` VALUES (27, '青海');
INSERT INTO `province` VALUES (33, '香港');
INSERT INTO `province` VALUES (9, '黑龙江');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gender` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthday` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `job` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hobby` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address_province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address_city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '洛必达', '123456789', '男', '2000-05-11', '4154@qq.com', '技工', NULL, '艾泽拉斯', NULL, NULL);
INSERT INTO `user` VALUES (2, '老布尼茨', '656266', '女', '1998-08-12', '45541231@qq.com', '码农', NULL, '南京', NULL, NULL);
INSERT INTO `user` VALUES (3, 'goduser', '456789', '男', '1978-07-17', 'asdas@sina.com', 'java工程师', NULL, '火星', NULL, NULL);
INSERT INTO `user` VALUES (11, 'robbidar', '82320689bjy', '男', '2020-04-19', 'werwe@qq.com', '学生', '电脑娱乐 影视娱乐 棋牌娱乐 读书看报 美酒佳肴 绘画书法', '江苏', '常州', '我是你的爸爸');
INSERT INTO `user` VALUES (12, 'Admin123', '82320689bjy', '男', '2020-04-10', '1231588@icloud.com', '白领', '电脑娱乐 影视娱乐', '江苏', '南京', '123');
INSERT INTO `user` VALUES (13, 'feng010101', 'feng010101', '女', '1970-10-10', 'werwe@qq.com', '白领', '影视娱乐 棋牌娱乐', '江苏', '淮安', '');
INSERT INTO `user` VALUES (14, '马士兵', 'eixita1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (15, '柯西', '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (16, '伯努利', '84561233', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (17, '华莱士', '68974513', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` VALUES (18, '张三', '123456789bjy', '男', '1985-05-06', '1231588@icloud.com', '白领', '电脑娱乐 影视娱乐 棋牌娱乐', '山东', '菏泽', '....');
INSERT INTO `user` VALUES (19, 'zhangsan', '82320689bjy', '男', '2020-04-10', '1231588@icloud.com', '学生', '电脑娱乐 影视娱乐', '广西', '桂林', '');
INSERT INTO `user` VALUES (20, 'motherfucker', '123456bjy', '男', '2000-09-15', '2722525929@qq.com', '白领', '电脑娱乐,影视娱乐,棋牌娱乐 电脑娱乐 影视娱乐 棋牌娱乐', NULL, NULL, 'gdx');
INSERT INTO `user` VALUES (21, '黄金素绝对好', '123456bjy', '女', '2020-05-08', 'werwe@qq.com', '学生', '影视娱乐,棋牌娱乐 影视娱乐 棋牌娱乐', '江苏', '无锡', '敌方的');
INSERT INTO `user` VALUES (22, '奥术大师', '123456bjy', '男', '2020-05-13', 'werwe@qq.com', '白领', '电脑娱乐,影视娱乐,棋牌娱乐,读书看报', '河南', '商丘', '舒服舒服');
INSERT INTO `user` VALUES (23, '绝对是不放假说得好', '123456bjy', '男', '2020-05-09', 'werwe@qq.com', '学生', '影视娱乐,棋牌娱乐,读书看报,美酒佳肴', '辽宁', '辽阳', '为士大夫撒发水电费');

SET FOREIGN_KEY_CHECKS = 1;
