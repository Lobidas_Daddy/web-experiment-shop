<%@ page import="com.pro.domain.Goods" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Avenger Munitions Supply</title>

  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    *{
      padding: 0px;
      margin: 0px;
      font-family: "微软雅黑 Light";
      font-weight: 700;
    }

    .thead th{
      font-size: 15px;
      text-align: center;
    }


  </style>
</head>

<body>

<div class="jumbotron" id="max_caption" style="display: none;margin-bottom: 0">
  <h1>Hello, world!</h1>
  <p>This is Avengers Munition Supply website which is one of the best e-commerce companies!</p>
  <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
</div>
<%--导航条--%>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="row">

      <div class="col-sm-2">
        <span id="commerce_cooper">商务合作:10086110119</span>
      </div>

      <div class="col-lg-4 text-primary col-lg-offset-2" style="font-size: 25px">
        AvengersMunitions.com
      </div>

      <div class="col-lg-2 text-primary col-lg-offset-2 text-right">
        <%@include file="welcome.jsp"%>
      </div>

    </div>
  </div>
</nav>

<%--按钮部分--%>
<div class="container">
  <div class="col-sm-1" style="padding-left: 0px">
    <a href="login.html" class="btn btn-success <c:if test="${loginUser != null}">disabled</c:if>" >
      登录<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
    </a>
  </div>

  <div class="col-sm-1" style="padding-left: 0px">
    <a href="register.html" class="btn-info btn">
      注册<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
    </a>
  </div>

  <div class="col-sm-1" style="padding-left: 0px">
    <a href="javascript:void(0)" class="btn-warning btn <c:if test="${loginUser == null}">disabled</c:if>" onclick="exit()" >
      退出<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
    </a>
  </div>

  <div class="col-sm-1 col-sm-offset-8">
    <a href="cart.jsp" class="btn-primary btn">
      <span class="badge" id="badge">${fn:length(cartMap)}</span>
      购物车
      <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
    </a>
  </div>
</div>

<%--表格部分--%>
<div class="container">
  <table class="table table-bordered table-hover table-striped text-center table-condensed thead text-primary">

    <caption style="font-size:30px" >Weapons Hot List</caption>

    <tr>
      <th>Rank</th>
      <th>Name</th>
      <th>Picture</th>
      <th>Price(No Discount)$</th>
      <th>Current$</th>
      <th>Type</th>
      <th>Operation</th>
    </tr>

    <c:forEach items="${pb.list}" var="Goods" varStatus="s">
      <tr>
        <td>${s.count}</td>
        <td>${Goods.goodsName}</td>
        <td><img src="${Goods.goodsPic}" alt="" width="200px" height="100px"></td>
        <td>${Goods.orgPrice}</td>
        <td>${Goods.nowPrice}</td>
        <td>${Goods.type}</td>
        <td><a href="javascript:void(0)" onclick="addOne(${Goods.id})" class="btn btn-danger">添加</a></td>
      </tr>
    </c:forEach>

  </table>

  <%--分页条--%>
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <li <c:if test="${pb.currentPage == 1}">class="disabled"</c:if> >
        <a <c:if test="${pb.currentPage != 1}">href="${pageContext.request.contextPath}/indexServer?currentPage=${pb.currentPage-1}"</c:if>>
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>

      <%--页面序号--%>
      <c:forEach var="i" begin="1" end="${pb.totalPage}">
        <li <c:if test="${pb.currentPage == i}">class="active"</c:if>>
          <a href="${pageContext.request.contextPath}/indexServer?currentPage=${i}">${i}</a>
        </li>
      </c:forEach>

      <li <c:if test="${pb.currentPage == pb.totalPage}">class="disabled"</c:if> >
        <a <c:if test="${pb.currentPage != pb.totalPage}">href="${pageContext.request.contextPath}/indexServer?currentPage=${pb.currentPage+1}"</c:if>>
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>
    </ul>

    <div class="text-primary" style="font-size: 20px">
      共有${pb.totalPage}页,${pb.totalCount}条数据
    </div>
  </nav>


  <div id="footer"></div>

</div>


</body>
<script src="js/auto_footer.js"></script>
<script>

  function addOne(id) {
    alert("成功添加!");
    location.href = "${pageContext.request.contextPath}/goods/addToCart?id="+id+"&currentPage=${pb.currentPage}";
  }

  window.setTimeout(function () {
    $("#max_caption").slideDown(3000);
  },3000);

  function exit() {
    if(confirm("您确定退出?")){
      location.href = "${pageContext.request.contextPath}/user/logout";
    }
  }
</script>
</html>
