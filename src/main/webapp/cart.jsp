<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Avengers Munitions Shopping Cart</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <style>
        *{
            font-weight: 700;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 text-primary">
                <span style="font-size: 20px">
                   Your Position：
                   <a href="${pageContext.request.contextPath}/indexServer" target="_self" class="btn-link">Home Page</a>&gt;
                   <a href="cart.jsp" class="btn-link">MyCart</a>
                </span>
            </div>


            <div class="col-lg-2 text-primary col-lg-offset-6 text-right">
                <%@include file="welcome.jsp"%>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    <form method="post" class="formStyle" action="${pageContext.request.contextPath}/goods/btnOperation" id="form1">

        <c:if test="${fn:length(cartMap) == 0}">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 text-danger text-center col-lg-offset-4">
                        <span style="font-size: 25px">Nothing Here!</span>
                    </div>
                </div>
            </div>
        </c:if>

        <table class="table table-striped table-bordered table-hover text-primary text-center">
            <caption class="text-center text-info" style="font-size: 30px">Shopping Cart</caption>
            <tr>
                <th class="text-center">
                    Select All
                    <input type="checkbox"id="selectAll" onclick="myCar()">
                </th>
                <th class="text-center">Picture</th>
                <th class="text-center">Name</th>
                <th class="text-center">Price($)</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Subtotal($)</th>
                <th class="text-center">Operation</th>
            </tr>


            <c:forEach varStatus="s" items="${cartMap}" var="goods">
                <tr>
                    <td><input type="checkbox" name="singleBox" onclick="selectSingle()" value="${goods.value.getId()}"></td>
                    <td><img src="${goods.value.getGoodsPic()}" width="200" height="100" alt=""></td>
                    <td>${goods.value.getGoodsName()}</td>
                    <td><span name="price">${goods.value.getNowPrice()}</span></td>
                    <td>
                    <span>
                        <a href="javascript:void(0)" name="minus" onclick="minusOne(${goods.value.getId()},${s.index})" class="btn btn-primary btn-sm">-</a>
                    </span>
                        <input type="text" name="quantity" value="${goods.value.getBuyNum()}" style="width: 50px">
                        <span>
                        <a href="${pageContext.request.contextPath}/goods/btnOperation?id=${goods.value.getId()}&flag=1" name="plus" class="btn btn-primary btn-sm">+</a>
                    </span>
                    </td>
                    <td>
                        <span name="subtotal">0</span>
                    </td>
                    <td><a href="javascript:void(0)" onclick="abandonGoods(${goods.value.getId()})" class="btn btn-warning">删除</a></td>
                </tr>
            </c:forEach>

        </table>
        <div class="container">
            <div class="row" id="total">
                <div class="col-lg-4 col-sm-offset-8 text-center" style="font-size: 20px">
                    Total Price(Exclude Freight): $<span id="sum_price" style="color: #ff8500">0</span>
                </div>
            </div>
        </div>

        <div class="container" style="padding-bottom: 10px">
            <div class="row">
                <div class="col-lg-2">
                    <a href="javascript:void(0)" onclick="deleteMulti()" class="btn btn-info">DeleteMulti</a>
                </div>

                <div class="col-lg-2 col-lg-offset-8" style="padding-left: 60px">
                    <a href="javascript:void(0)" class="btn btn-primary" onclick="buyAtOnce()">Buy Now!</a>
                </div>
            </div>
        </div>

    </form>

    <div id="footer"></div>
</div>


</body>
<script src="js/auto_footer.js"></script>
<script>
    //刚进入页面之时就进行一次计算
    window.onload = change;
    //复选框
    var checkBox = document.getElementsByName("singleBox");
    var selectall = document.getElementById("selectAll");

    //商品信息
    var price = document.getElementsByName("price");
    var quantity = document.getElementsByName("quantity");
    var subtotal = document.getElementsByName("subtotal");
    var countTotal = 0; //总价计数器
    var sum_price = document.getElementById("sum_price");  //总价文本内容

    //初始化  控制  设置所有输入框为只读状态
    for(let i=0;i<quantity.length;i++){
        quantity[i].readOnly = true;
    }

    //减号按钮的校验函数
    function minusOne(id,index) {
        let temp = parseInt(quantity[index].value);
        //如果商品数量为1的话那么就禁用减号按钮
        if (temp == 1) {
            return;
        }
        //把商品的id号传给servlet
        console.log(id);
        location.href = "${pageContext.request.contextPath}/goods/btnOperation?id=" + id + "&flag=0";
    }

    //计算总价
    function change() {
        let count = 0;  //临时总价计数器
        for(let i=0;i<price.length;i++){
            let temp = Math.floor(price[i].innerText * quantity[i].value*100)/100;
            subtotal[i].innerText = temp;
            count += temp;
        }
        count = Math.floor(count * 100)/100;
        countTotal = count;
        sum_price.innerText = countTotal+"";
    }

    //全选
    function myCar() {
        for(let i=0;i<checkBox.length;i++){
            checkBox[i].checked = selectall.checked;
        }
    }

    function selectSingle() {
        //假设已经全选
        selectall.checked = true;

        for(let i=0;i<checkBox.length;i++){
            if(!checkBox[i].checked) {  //只要有一个没有选中
                selectall.checked = false;  //设为false
            }
        }
    }

    //将删除的商品信息传输给servlet 在list集合里面删除此物件 然后重新刷新页面
    //再用foreach 循环迭代表格元素
    function deleteMulti(){
        if(checkForm()){
            if(confirm("Sure about that?")){
                document.getElementById("form1").submit();
            }
        }
    }

    //表单校验看是否存在选中的框子 若没有那么不提交   函数要复用
    function checkForm(){
        for(let i=0;i<checkBox.length;i++){
            if(checkBox[i].checked){
                return true;
            }
        }
        alert("At least one selected");
        return false;
    }

    //同理提交给servlet删除单个物件  重新刷新页面  foreach循环迭代表格元素
    function abandonGoods(id) {
        let flag = confirm("Are you sure to abandon this?");
        if (flag) {
            //删除信息提交给服务器  创建一个处理删除信息的servlet
            location.href = "${pageContext.request.contextPath}/goods/btnOperation?id="+id+"&flag=2";
        }
    }

    function buyAtOnce() {
        if(!checkBox.length){
            alert("没有添加商品");
            return;
        }

        if(${loginUser == null}){
            location.href = "login.html";
            return;
        }

        if(confirm("确定购买吗?")){
            location.href = "${pageContext.request.contextPath}/goods/buyNow?totalPrice="+countTotal;
        }
    }

</script>

</html>
