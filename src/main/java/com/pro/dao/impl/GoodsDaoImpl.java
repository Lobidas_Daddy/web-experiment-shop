package com.pro.dao.impl;

import com.pro.dao.GoodsDao;
import com.pro.domain.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GoodsDaoImpl implements GoodsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //展示所有商品数据
    @Override
    public List<Goods> selectAll() {

        String sql = "select * from goods";

        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Goods.class));
    }

    //添加新得商品
    @Override
    public void insertGoods(Goods goods) {

        String sql = "insert into goods values(?,?,?,?,?,?,?,?,?)";

        jdbcTemplate.update(sql,null,goods.getGoodsName(),goods.getGoodsPic(),goods.getOrgPrice(),goods.getNowPrice(),
                goods.getType(),goods.getComments(),goods.getGoodsNum(),goods.getBuyNum());
    }

    @Override
    public List<Goods> findByPage(int currentPage, int pageSize) {

        String sql = "select * from goods limit ? , ?";

        List<Goods> goodsList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Goods>(Goods.class), (currentPage - 1) * pageSize, pageSize);

        return goodsList;
    }

    @Override
    public int findTotalCount() {

        String sql = "select count(*) from goods ";

        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

}
