package com.pro.dao.impl;

import com.pro.dao.OrderDao;
import com.pro.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void addOrder(Order order) {

        String sql = "insert into orderlist values(?,?,?,?,?)";

        jdbcTemplate.update(sql,null,order.getOrderId(),order.getUserId(),order.getOrderTime(),order.getTotalPrice());

    }

    @Override
    public void addOrderDetail(List<Object[]> list) {

        String sql = "insert into orderDetail values(?,?,?,?,?)";

        jdbcTemplate.batchUpdate(sql,list);
    }

}
