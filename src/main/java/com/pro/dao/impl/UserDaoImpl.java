package com.pro.dao.impl;

import com.pro.dao.UserDao;
import com.pro.domain.Admin;
import com.pro.domain.City;
import com.pro.domain.Province;
import com.pro.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    //获取连接池
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //查询会员是否存在
    @Override
    public User getUser(User user) {

        String sql = "select * from user where username = ? and password = ?";

        User loginUser = null;

        try {
            loginUser = jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<>(User.class),user.getUsername(),user.getPassword());
        } catch (DataAccessException e) {
            //do nothing
        }

        return loginUser;
    }
    //判断用户名是否存在
    @Override
    public boolean isUserExisted(String username) {
        String sql = "select * from user where username = ?";
        User user = null;
        try {
            user = jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<>(User.class),username);
        } catch (DataAccessException e) {
            //do nothing
        }

        if(user != null){
            //表示存在
            return true;
        }

        return false;
    }

    //展示所有省份数据
    @Override
    public List<Province> showAllProvinces() {
        String sql = "select * from province";
        List<Province> provinceList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Province>(Province.class));
        return provinceList;
    }

    //展示对应省份的地级市数据
    @Override
    public List<City> showCitiesByProvince(String province) {
        String sql = "select * from city where province = ?";
        List<City> cityList = null;
        try {
            cityList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<City>(City.class), province);
        } catch (DataAccessException e) {
            //do nothing
        }
        return cityList;
    }

    //会员注册操作
    @Override
    public void userRegister(User user) {
        String sql = "insert into user values(?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql,null,user.getUsername(),user.getPassword(),user.getGender(),user.getBirthday(),user.getEmail(),
                user.getJob(),user.getHobby(),user.getProvince(),user.getCity(),user.getIntroduction());
    }

    //判断管理员用户是否存在
    @Override
    public Admin getAdmin(Admin admin) {
        String sql = "select * from administrator where username = ? and password = ?";
        Admin loginAdmin = null;

        try {
            loginAdmin = jdbcTemplate.queryForObject(sql,new BeanPropertyRowMapper<Admin>(Admin.class),admin.getUsername(),admin.getPassword());
        } catch (DataAccessException e) {
           //do nothing
        }
        return loginAdmin;
    }
}
