package com.pro.dao;

import com.pro.domain.Goods;

import java.util.List;

public interface GoodsDao {

    //查询所有的商品
    List<Goods> selectAll();

    //添加商品
    void insertGoods(Goods goods);

    //分页查询
    List<Goods> findByPage(int currentPage, int pageSize);

    //查询所有的数据条数
    int findTotalCount();

}
