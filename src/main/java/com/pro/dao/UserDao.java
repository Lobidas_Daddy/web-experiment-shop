package com.pro.dao;

import com.pro.domain.Admin;
import com.pro.domain.City;
import com.pro.domain.Province;
import com.pro.domain.User;

import java.util.List;

public interface UserDao {
    User getUser(User user);

    boolean isUserExisted(String username);

    List<Province> showAllProvinces();

    List<City> showCitiesByProvince(String province);

    void userRegister(User user);

    Admin getAdmin(Admin admin);
}
