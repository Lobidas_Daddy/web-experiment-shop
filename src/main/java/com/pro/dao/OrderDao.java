package com.pro.dao;

import com.pro.domain.Order;

import java.util.List;

public interface OrderDao {

    //添加订单
    void addOrder(Order order);

    void addOrderDetail(List<Object[]> list);

}
