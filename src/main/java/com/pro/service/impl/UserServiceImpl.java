package com.pro.service.impl;

import com.pro.dao.UserDao;
import com.pro.domain.Admin;
import com.pro.domain.City;
import com.pro.domain.Province;
import com.pro.domain.User;
import com.pro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getUser(User user) {

        return userDao.getUser(user);
    }

    @Override
    public boolean isUserExisted(String username) {
        return userDao.isUserExisted(username);
    }

    @Override
    public List<Province> showAllProvinces() {
        return userDao.showAllProvinces();
    }

    @Override
    public List<City> showCitiesByProvince(String province) {
        return userDao.showCitiesByProvince(province);
    }

    @Override
    public void register(User user) {
        userDao.userRegister(user);
    }

    @Override
    public Admin getAdmin(Admin admin) {
        return userDao.getAdmin(admin);
    }
}
