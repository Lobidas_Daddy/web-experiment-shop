package com.pro.service.impl;

import com.pro.dao.GoodsDao;
import com.pro.dao.OrderDao;
import com.pro.domain.Goods;
import com.pro.domain.Order;
import com.pro.domain.PageBean;
import com.pro.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<Goods> selectAll() {
        return goodsDao.selectAll();
    }

    @Override
    public void updateGoods(Goods goods) {
        goodsDao.insertGoods(goods);
    }

    @Override
    public PageBean<Goods> getPageBean(String start, String rows) {
        //当前页  每页显示的条数
        int currentPage = Integer.parseInt(start);
        int pageSize = Integer.parseInt(rows);

        //封装pageBean
        PageBean<Goods> pageBean = new PageBean<>();

        //设置当前页
        pageBean.setCurrentPage(currentPage);

        //设置每页的条数
        pageBean.setPageSize(pageSize);

        //设置每页显示的数据  显示的数据序列 limit (currentPage-1)*pageSize ,pageSize  等差数列
        List<Goods> list = goodsDao.findByPage(currentPage,pageSize);
        pageBean.setList(list);

        //设置总条数
        int totalCount = goodsDao.findTotalCount();
        pageBean.setTotalCount(totalCount);

        //设置总页数
        int totalPage = (totalCount % pageSize == 0) ? (totalCount / pageSize) : (totalCount / pageSize + 1);
        pageBean.setTotalPage(totalPage);

        return pageBean;
    }

    @Override
    public void addOrder(Order order) {

        orderDao.addOrder(order);
    }

    @Override
    public void addOrderDetail(List<Object[]> list) {

        orderDao.addOrderDetail(list);
    }
}
