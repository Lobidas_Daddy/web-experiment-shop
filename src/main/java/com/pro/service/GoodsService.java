package com.pro.service;

import com.pro.domain.Goods;
import com.pro.domain.Order;
import com.pro.domain.PageBean;

import java.util.List;

public interface GoodsService {
    List<Goods> selectAll();

    void updateGoods(Goods goods);

    PageBean<Goods> getPageBean(String start, String rows);

    void addOrder(Order order);

    void addOrderDetail(List<Object[]> list);
}
