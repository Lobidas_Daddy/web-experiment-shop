package com.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Goods {
    private Integer id;
    private String goodsName;
    private String goodsPic;
    private Double orgPrice;
    private Double nowPrice;
    private String type;
    private Integer comments;
    private Integer goodsNum;
    private Integer buyNum;

}
