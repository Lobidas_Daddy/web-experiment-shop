package com.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultInfo implements Serializable {

    private boolean flag;//后端返回结果为true表示正常 false表示异常
    private Object data;//返回的数据
    private String errorMsg;//返回的错误信息


}
