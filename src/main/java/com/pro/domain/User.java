package com.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
public class User {
    private int id;
    private String username;
    private String password;
    private String gender;
    private String birthday;
    private String email;
    private String job;
    private String hobby;
    private String province;
    private String city;
    private String introduction;


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User() {

    }
}
