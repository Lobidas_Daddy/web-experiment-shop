package com.pro.domain;


import lombok.Data;

import java.util.UUID;
@Data

public class Order {
    private String orderId;
    private String userId;
    private String orderTime;
    private Double totalPrice;

    public Order(String userId, String orderTime, Double totalPrice) {
        //生成订单号
        this.orderId = createOrderId();
        this.userId = userId;
        this.orderTime = orderTime;
        this.totalPrice = totalPrice;
    }


    private String createOrderId(){
        String uuid = UUID.randomUUID().toString().replace("-", "");

        return uuid;
    }

}
