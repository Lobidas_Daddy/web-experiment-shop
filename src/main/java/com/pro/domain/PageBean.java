package com.pro.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页实体类  泛型类
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageBean<T>{

    private int totalCount;//数据总条数

    private int pageSize;//单页数据条数

    private List<T> list;//单页显示的数据

    private int totalPage;//总页数

    private int currentPage;//当前页号

}
