package com.pro.web.controller;

import com.pro.domain.Goods;
import com.pro.domain.Order;
import com.pro.domain.ResultInfo;
import com.pro.domain.User;
import com.pro.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/goods")
public class GoodsController{

    @Autowired
    private GoodsService goodsService;

    @RequestMapping("/addToCart")
    public String addToCart(HttpServletRequest request,String currentPage,String id){

        //存放选购物件的map集合
        Map<Integer, Goods> cartMap = (Map<Integer, Goods>)request.getSession().getAttribute("cartMap");
        if(cartMap == null){
            cartMap = new HashMap<Integer, Goods>();
        }

        //所有商品的集合
        Map<Integer,Goods> goodsMap = (Map<Integer,Goods>)request.getSession().getAttribute("goodsMap");
        Integer id2 = Integer.valueOf(id); //获取商品id号

        Goods temp = (Goods) cartMap.get(id2);  //临时物件对象

        if (temp == null) {
            //如果购物车当中不存在此物件
            temp = (Goods) goodsMap.get(id2);
            temp.setBuyNum(1);
            cartMap.put(id2, temp);
        } else {
            temp.setBuyNum(temp.getBuyNum() + 1);
            cartMap.put(id2, temp);
        }

        request.getSession().setAttribute("cartMap",cartMap);

        return "redirect:/indexServer?currentPage="+currentPage;

    }

    @RequestMapping("/btnOperation")
    public String btnOperation(HttpServletRequest request,String flag,Integer id,String[] singleBox){
        //存放选购物件的map集合
        Map<Integer, Goods> cartMap = (Map<Integer, Goods>)request.getSession().getAttribute("cartMap");

        //flag 获取操作详情  增加数量或者减少数量 增加为1  减少为0
        Goods temp = (Goods) cartMap.get(id);  //临时物件对象

        if("1".equals(flag)){  //增加商品
            temp.setBuyNum(temp.getBuyNum()+1);
            cartMap.put(id,temp);
        }else if("0".equals(flag)){ //减少商品
            temp.setBuyNum(temp.getBuyNum()-1);
            cartMap.put(id,temp);
        }else if("2".equals(flag)){  //删除商品
            cartMap.remove(id);
        }else { //删除多个商品
            for(String str : singleBox){
                cartMap.remove(Integer.valueOf(str));
            }
        }

        request.getSession().setAttribute("cartMap",cartMap);

        return "redirect:/cart.jsp";
    }

    //上架新品
    @RequestMapping("/update")
    @ResponseBody
    public ResultInfo update(Goods goods){

        //返回提示信息对象
        ResultInfo resultInfo = new ResultInfo();

        resultInfo.setFlag(true);

        goodsService.updateGoods(goods);

        //回显数据到前台
        return resultInfo;
    }

    //立即购买
    @RequestMapping("/buyNow")
    public String buyNow(HttpServletRequest request) throws IOException {

        Map<Integer, Goods> cartMap = (Map<Integer, Goods>)request.getSession().getAttribute("cartMap");

        Set<Integer> keys = cartMap.keySet();

        //获取总价
        double total = 0.00;
        String totalPrice = request.getParameter("totalPrice");
        total = Double.parseDouble(totalPrice);

        System.out.println("totalPrice = " + totalPrice);

        //获取用户的信息
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        String username = loginUser.getUsername();

        //获取时间
        String timeStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        //创建订单对象
        Order order = new Order(username,timeStr,total);
        String orderId = order.getOrderId();

        //插入数据
        goodsService.addOrder(order);

        //创建一个list集合用于存放订单细节信息
        List<Object[]> orderDetailList = new ArrayList<>();

        for(Integer key : keys){
            Goods goods = cartMap.get(key);
            orderDetailList.add(new Object[]{null,orderId,key,goods.getNowPrice(),goods.getBuyNum()});
        }

        goodsService.addOrderDetail(orderDetailList);

        cartMap.keySet().removeIf(key -> key != 0);

        request.setAttribute("cartMap",cartMap);

        return "redirect:/success_paid.html";

    }

}
