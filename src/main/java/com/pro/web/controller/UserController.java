package com.pro.web.controller;

import com.pro.domain.*;
import com.pro.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController{

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    @ResponseBody
    public ResultInfo login(HttpServletRequest request, HttpServletResponse response,String username,String password,String check_code,String next_time){

        //返回的信息对象
        ResultInfo resultInfo = new ResultInfo();

        //先获取验证码信息
        String validateCode = (String) request.getSession().getAttribute("validateCode");

        //比对验证码
        if(!check_code.equalsIgnoreCase(validateCode) || validateCode == null ){
            resultInfo.setErrorMsg("验证码错误!");
            resultInfo.setFlag(false);

            return resultInfo;
        }

        User user = new User(username,password); //使用获取的用户名密码生成一个用户对象

        User loginUser = userService.getUser(user); //判断是否存在

        if(loginUser == null){
            //表示登录失败
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("用户名或密码错误~");

        }else {
            resultInfo.setFlag(true);
            //登陆成功  将信息保存到session
            request.getSession().setAttribute("loginUser",loginUser);

            if("1".equals(next_time)){
                //点击自动登录
                Cookie cookie = new Cookie("autoLoginCookie",loginUser.getUsername()+"-"+loginUser.getPassword());
                cookie.setMaxAge(60*60*24*7);//7天的有效期
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }

        //实现一次性验证码
        request.getSession().removeAttribute("validateCode");

        return resultInfo;
    }

    //用户注册
    @RequestMapping("/register")
    public String register(User user){

        System.out.println("user = " + user);

        userService.register(user);

        return "redirect:/login.html";
    }

    //判断用户是否重复 返回boolean
    @RequestMapping("/isUserExisted")
    @ResponseBody
    public ResultInfo isUserExisted(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //获取用户名
        String username = request.getParameter("username");
        //判断是否重复
        ResultInfo resultInfo = new ResultInfo();

        boolean flag = userService.isUserExisted(username);
        resultInfo.setFlag(flag);

        return resultInfo;
    }

    //展示省份信息
    @RequestMapping("/showProvinceName")
    @ResponseBody
    public List<Province> showProvinceName(HttpServletRequest request, HttpServletResponse response) throws IOException{

        return userService.showAllProvinces();
    }

    //根据省份名称展示地级市名称
    @RequestMapping("/showCityNameByProvince")
    @ResponseBody
    public List<City> showCityNameByProvince(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String province = request.getParameter("province");

        return userService.showCitiesByProvince(province);
    }

    //退出登录操作
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().removeAttribute("loginUser");

        Cookie cookie = new Cookie("autoLoginCookie","");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);

        return "redirect:/indexServer";
    }

    //管理员登录操作
    @RequestMapping("/adminLogin")
    @ResponseBody
    public ResultInfo adminLogin(HttpServletRequest request,String check_code,String username,String password){

        //返回的信息对象
        ResultInfo resultInfo = new ResultInfo();

        //先获取验证码信息
        String validateCode = (String) request.getSession().getAttribute("validateCode");

        //比对验证码
        if(!check_code.equalsIgnoreCase(validateCode) || validateCode == null ){
            resultInfo.setErrorMsg("验证码错误!");
            resultInfo.setFlag(false);

            return resultInfo;
        }

        //创建管理员对象
        Admin admin = new Admin(username,password);

        //校验
        Admin loginAdmin = userService.getAdmin(admin);

        if(loginAdmin == null){
            //登录失败
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("用户名或密码错误~");
        }else{
            //登录成功
            resultInfo.setFlag(true);
            request.getSession().setAttribute("loginAdmin",loginAdmin);
        }

        //一次性验证码
        request.getSession().removeAttribute("validateCode");
        //回显数据到前台
        return  resultInfo;

    }

}
