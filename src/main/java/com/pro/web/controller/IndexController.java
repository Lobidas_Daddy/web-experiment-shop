package com.pro.web.controller;

import com.pro.domain.Goods;
import com.pro.domain.PageBean;
import com.pro.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private GoodsService goodsService;

    @RequestMapping("/indexServer")
    protected String indexServe(HttpServletRequest request,String currentPage,String rows) throws IOException {
        request.setCharacterEncoding("utf-8");//解决中文乱码问题

        List<Goods> list = goodsService.selectAll();
        //把商品存到map中去
        if(request.getSession().getAttribute("goodsMap") == null){
            Map<Integer,Goods> goodsMap = new HashMap<>();
            for(Goods goods : list){
                goodsMap.put(goods.getId(),goods);
            }
            request.getSession().setAttribute("goodsMap",goodsMap);
        }

        //默认是第一页
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }

        //默认是一页展示5条
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        PageBean<Goods> pageBean = goodsService.getPageBean(currentPage,rows);

        request.setAttribute("pb",pageBean);

        return "forward:/index.jsp";
    }

}
