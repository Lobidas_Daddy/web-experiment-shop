package com.pro.web.filter;

import com.pro.domain.User;
import com.pro.service.impl.UserServiceImpl;
import com.pro.service.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class AutoLoginFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        //将父接口转化成子接口
        HttpServletRequest  httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        //获取登录信息
        Object loginUser = httpRequest.getSession().getAttribute("loginUser");

        //用户未登录
        if(loginUser == null){
            //判断cookies之中是否含有登录信息
            Cookie[] cookies = httpRequest.getCookies();

            if(cookies != null && cookies.length > 0){
                //遍历cookies
                for(Cookie cookie : cookies){
                    if("autoLoginCookie".equals(cookie.getName())){
                        //存在cookie
                        String value = cookie.getValue();

                        //封装user
                        User auto_login_user = new User();
                        auto_login_user.setUsername(value.split("-")[0]);
                        auto_login_user.setPassword(value.split("-")[1]);

                        //调用service层对象查询数据库登录信息是否存在
                        UserService userService = new UserServiceImpl();
                        User user = userService.getUser(auto_login_user);

                        if(user != null){
                            httpRequest.getSession().setAttribute("loginUser",user);
                        }

                        //跳出循环
                        break;
                    }
                }
            }

        }
        chain.doFilter(request, response);
    }

    public void destroy() {
    }

}
