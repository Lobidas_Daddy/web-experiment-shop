package com.pro.web.filter;

import com.pro.domain.Admin;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/update_goods.html"},initParams = {@WebInitParam(name="backUrl",value = "login_admin.html")})
public class LoginFilter implements Filter {
    String backUrl;//如果没有登录那么就跳转到管理员登录界面

    public void init(FilterConfig config) throws ServletException {

        if(config.getInitParameter("backUrl") != null){ //从过滤器的配置中获得初始化参数，如果没有就使用默认值 index.jsp
            backUrl = config.getInitParameter("backUrl");
        }else{
            backUrl = "index.jsp";
        }
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        //将父接口转化为子接口
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        //获取管理员登录信息
        Admin loginAdmin = (Admin) httpRequest.getSession().getAttribute("loginAdmin");

        if(loginAdmin == null){
            httpResponse.sendRedirect(backUrl);
        }else{
            chain.doFilter(request, response);
        }

    }

    public void destroy() {
    }

}
